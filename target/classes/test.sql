--This script is used for unit test cases, DO NOT CHANGE!

DROP TABLE IF EXISTS User;

CREATE TABLE User (UserId LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
 UserName VARCHAR(30) NOT NULL,
 EmailId VARCHAR(30) NOT NULL);

CREATE UNIQUE INDEX index_user on User(UserName,EmailId);

INSERT INTO User (UserName, EmailId) VALUES ('demouser1','demouser1@gmail.com');
INSERT INTO User (UserName, EmailId) VALUES ('demouser2','demouser2@gmail.com');
INSERT INTO User (UserName, EmailId) VALUES ('demouser3','demouser3gmail.com');

DROP TABLE IF EXISTS Account;

CREATE TABLE Account (AccountId LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
UserName VARCHAR(30),
Balance DECIMAL(19,4),
CurrencyCode VARCHAR(30)
);

CREATE UNIQUE INDEX index_account on Account(UserName,CurrencyCode);

INSERT INTO Account (UserName,Balance,CurrencyCode) VALUES ('demouser1',1000.0000,'INR');
INSERT INTO Account (UserName,Balance,CurrencyCode) VALUES ('demouser2',20000.0000,'INR');
INSERT INTO Account (UserName,Balance,CurrencyCode) VALUES ('demouser3',20500.0000,'INR');
