
                                               
##Revolut Money Transfer RESTful Api

The goal of this project is to design and implement a RESTful API (including data model and the backing implementation) for money transfers between accounts.


##Steps to build and run

```
> mvn clean install
```
```
> mvn exec:java
```

##Rest Api Route(s)

```
 GET : 	    http://localhost:8080/user/{userName}
 
 GET :      http://localhost:8080/user/list
 
 POST :     http://localhost:8080/user/createUser
 
 PUT :      http://localhost:8080/user/update/{userId}
 
 DELETE :   http://localhost:8080/user/delete/{userId}
 
 GET : 	    http://localhost:8080/account/list
 
 GET :      http://localhost:8080/account/{accountId}
 
 GET :      http://localhost:8080/account/balance/{accountId}
 
 PUT :      http://localhost:8080/account/createAccount
 
 PUT :      http://localhost:8080/account/deposit/{accountId}/{amount}
 
 PUT :      http://localhost:8080/account/withdraw/{accountId}/{amount}
 
 DELETE :   http://localhost:8080/account/delete/{accountId}
 
 POST :     http://localhost:8080/account/transferAmount
```




##JSON example for API’s


Create user :
```
{
	“userName” : “demoTestUser”,
	“emailId” : “demoTestUser@gmail.com”
}
```
   

Create user account :
```
{
	“userName” : “demoTestUser”,
	“balance” : 10000.0000,
	“currencyCode” : “INR”
}
```
     

Update user account :
```
{
	“userName” : “name_to_update”,
	“emailId” : “new_emailid”
}
```
     

Transfer amount from one user to other:
```
{
	"currencyCode" : "INR",
	"amount" : 50,
	"from" : "1",
	"to":"2"
}
```




