package com.moneytransfer.exception;

public class RevolutMoneyException extends Exception {

	private static final long serialVersionUID = 1L;

	public RevolutMoneyException(String errMsg) {
		super(errMsg);
	}
}
