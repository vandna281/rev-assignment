package com.moneytransfer.dao;

import java.math.BigDecimal;
import java.util.List;

import com.moneytransfer.exception.RevolutMoneyException;
import com.moneytransfer.model.AccountModel;
import com.moneytransfer.model.UserTransaction;


public interface AccountDAO {

    List<AccountModel> getAllAccounts() throws RevolutMoneyException;
    AccountModel getAccountById(long accountId) throws RevolutMoneyException;
    long createAccount(AccountModel account) throws RevolutMoneyException;
    int deleteAccountById(long accountId) throws RevolutMoneyException;
    int updateAccountBalance(long accountId, BigDecimal deltaAmount) throws RevolutMoneyException;
    int transferAccountBalance(UserTransaction userTransaction) throws RevolutMoneyException;
}
