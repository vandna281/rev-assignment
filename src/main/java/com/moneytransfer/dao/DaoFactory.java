package com.moneytransfer.dao;

public abstract class DaoFactory {

	public static final int H2 = 1;

	public abstract UserDAO getUserDAO();

	public abstract AccountDAO getAccountDAO();

	public abstract void populateTestData();

	public static DaoFactory getDAOFactory(int factoryCode) {

		switch (factoryCode) {
		case H2:
			return new DaoFactoryHelper();
		default:
			// by default using H2 in memory database
			return new DaoFactoryHelper();
		}
	}
}
