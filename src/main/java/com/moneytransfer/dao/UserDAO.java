package com.moneytransfer.dao;

import com.moneytransfer.exception.RevolutMoneyException;
import com.moneytransfer.model.User;

import java.util.List;

public interface UserDAO {
	
	List<User> getAllUsers() throws RevolutMoneyException;

	User getUserById(long userId) throws RevolutMoneyException;

	User getUserByName(String userName) throws RevolutMoneyException;

	/**
	 * @param user:
	 * user to be created
	 * @return userId generated from insertion. return -1 on error
	 */
	long insertUser(User user) throws RevolutMoneyException;

	int updateUser(Long userId, User user) throws RevolutMoneyException;

	int deleteUser(long userId) throws RevolutMoneyException;

}
