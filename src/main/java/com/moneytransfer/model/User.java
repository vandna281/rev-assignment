package com.moneytransfer.model;


import com.fasterxml.jackson.annotation.JsonProperty;

public class User {

	@JsonProperty(required = true)
    private long userId ;


    @JsonProperty(required = true)
    private String userName;


    @JsonProperty(required = true)
    private String emailId;


    public User() {}

    public User(String userName, String emailId) {
        this.userName = userName;
        this.emailId = emailId;
    }

    public User(long userId, String userName, String emailId) {
        this.userId = userId;
        this.userName = userName;
        this.emailId = emailId;
    }

    public long getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public String getEmailId() {
        return emailId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (userId != user.userId) return false;
        if (!userName.equals(user.userName)) return false;
        return emailId.equals(user.emailId);

    }

    @Override
    public int hashCode() {
        int result = (int) (userId ^ (userId >>> 32));
        result = 31 * result + userName.hashCode();
        result = 31 * result + emailId.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", emailAddress='" + emailId + '\'' +
                '}';
    }
}
