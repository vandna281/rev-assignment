package com.moneytransfer.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.moneytransfer.dao.DaoFactory;
import com.moneytransfer.exception.RevolutMoneyException;
import com.moneytransfer.model.AccountModel;
import com.moneytransfer.model.MoneyUtil;
import com.moneytransfer.model.UserTransaction;

/**
 * Account Service 
 */
@Path("/account")
@Produces(MediaType.APPLICATION_JSON)
public class AccountService {
	
    private final DaoFactory daoFactory = DaoFactory.getDAOFactory(DaoFactory.H2);
    
    private static Logger log = Logger.getLogger(AccountService.class);

    
    /**
     * Find all accounts
     * @return
     * @throws RevolutMoneyException
     */
    @GET
    @Path("/list")
    public List<AccountModel> getAllAccounts() throws RevolutMoneyException {
    	log.info("Request Received to get all accounts ");
        return daoFactory.getAccountDAO().getAllAccounts();
    }

    /**
     * Find by account id
     * @param accountId
     * @return
     * @throws RevolutMoneyException
     */
    @GET
    @Path("/{accountId}")
    public AccountModel getAccount(@PathParam("accountId") long accountId) throws RevolutMoneyException {
    	log.info("Request Received to get account by Id " + accountId);
        return daoFactory.getAccountDAO().getAccountById(accountId);
    }
    
    /**
     * 
     * @param accountId
     * @return
     * @throws RevolutMoneyException
     */
    @GET
    @Path("/balance/{accountId}")
    public BigDecimal getBalance(@PathParam("accountId") long accountId) throws RevolutMoneyException {
    	log.info("Request Received to get balane for account " + accountId);
        final AccountModel account = daoFactory.getAccountDAO().getAccountById(accountId);

        if(account == null){
            throw new WebApplicationException("Account not found", Response.Status.NOT_FOUND);
        }
        return account.getBalance();
    }
    
    /**
     * Create Account
     * @param account
     * @return
     * @throws RevolutMoneyException
     */
    @PUT
    @Path("/createAccount")
    public AccountModel createAccount(AccountModel account) throws RevolutMoneyException {
    	log.info("Request Received to create account");
        final long accountId = daoFactory.getAccountDAO().createAccount(account);
        return daoFactory.getAccountDAO().getAccountById(accountId);
    }

    /**
     * Deposit amount by account Id
     * @param accountId
     * @param amount
     * @return
     * @throws RevolutMoneyException
     */
    @PUT
    @Path("/deposit/{accountId}/{amount}")
    public AccountModel deposit(@PathParam("accountId") long accountId,@PathParam("amount") BigDecimal amount) throws RevolutMoneyException {

        if (amount.compareTo(MoneyUtil.zeroAmount) <=0){
            throw new WebApplicationException("Invalid Deposit amount", Response.Status.BAD_REQUEST);
        }

        daoFactory.getAccountDAO().updateAccountBalance(accountId,amount.setScale(4, RoundingMode.HALF_EVEN));
        return daoFactory.getAccountDAO().getAccountById(accountId);
    }

    /**
     * Withdraw amount by account Id
     * @param accountId
     * @param amount
     * @return
     * @throws RevolutMoneyException
     */
    @PUT
    @Path("/withdraw/{accountId}/{amount}")
    public AccountModel withdraw(@PathParam("accountId") long accountId,@PathParam("amount") BigDecimal amount) throws RevolutMoneyException {

        if (amount.compareTo(MoneyUtil.zeroAmount) <=0){
            throw new WebApplicationException("Invalid Deposit amount", Response.Status.BAD_REQUEST);
        }
        BigDecimal delta = amount.negate();
        if (log.isDebugEnabled())
            log.debug("Withdraw service: delta change to account  " + delta + " Account ID = " +accountId);
        daoFactory.getAccountDAO().updateAccountBalance(accountId,delta.setScale(4, RoundingMode.HALF_EVEN));
        return daoFactory.getAccountDAO().getAccountById(accountId);
    }


    /**
     * Delete amount by account Id
     * @param accountId
     * @param amount
     * @return
     * @throws RevolutMoneyException
     */
    @DELETE
    @Path("/delete/{accountId}")
    public Response deleteAccount(@PathParam("accountId") long accountId) throws RevolutMoneyException {
        int deleteCount = daoFactory.getAccountDAO().deleteAccountById(accountId);
        if (deleteCount == 1) {
            return Response.ok("Account deleted successfully").build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
    
    /**
	 * Transfer fund between two accounts.
	 * @param transaction
	 * @return
	 * @throws RevolutMoneyException
	 */
	@POST
	@Path("/transferAmount")
	public Response transferFund(UserTransaction transaction) throws RevolutMoneyException {

		String currency = transaction.getCurrencyCode();
		if (MoneyUtil.INSTANCE.validateCcyCode(currency)) {
			int updateCount = daoFactory.getAccountDAO().transferAccountBalance(transaction);
			if (updateCount == 2) {
				return Response.ok("Transfer Successfull").build();
			} else {
				// transaction failed
				throw new WebApplicationException("Transaction failed", Response.Status.BAD_REQUEST);
			}

		} else {
			throw new WebApplicationException("Currency Code Invalid ", Response.Status.BAD_REQUEST);
		}
	}

}
