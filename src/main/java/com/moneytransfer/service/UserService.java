package com.moneytransfer.service;

import com.moneytransfer.dao.DaoFactory;
import com.moneytransfer.exception.RevolutMoneyException;
import com.moneytransfer.model.User;

import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
public class UserService {

	private final DaoFactory daoFactory = DaoFactory.getDAOFactory(DaoFactory.H2);

	private static Logger log = Logger.getLogger(UserService.class);

	/**
	 * Find by userName
	 * 
	 * @param userName
	 * @return
	 * @throws Exception
	 */
	@GET
	@Path("/{userName}")
	public User getUserByName(@PathParam("userName") String userName) throws Exception {
		log.info("Request Received for get User by Name " + userName);
		final User user = daoFactory.getUserDAO().getUserByName(userName);
		if (user == null) {
			throw new Exception("User Not Found");
		}
		return user;
	}

	/**
	 * Find by all
	 * 
	 * @param userName
	 * @return
	 * @throws RevolutMoneyException
	 */
	@GET
	@Path("/list")
	public List<User> getAllUsers() throws RevolutMoneyException {
		log.info("Request Received to get all users");
		return daoFactory.getUserDAO().getAllUsers();
	}

	/**
	 * Create User
	 * 
	 * @param user
	 * @return
	 * @throws RevolutMoneyException
	 */
	@POST
	@Path("/createUser")
	public User createUser(User user) throws RevolutMoneyException {
		log.info("Request Received to create User by Name " + user.getUserName());
		if (daoFactory.getUserDAO().getUserByName(user.getUserName()) != null) {
			throw new WebApplicationException("User name already exist", Response.Status.BAD_REQUEST);
		}
		final long uId = daoFactory.getUserDAO().insertUser(user);
		return daoFactory.getUserDAO().getUserById(uId);
	}

	/**
	 * Find by User Id
	 * 
	 * @param userId
	 * @param user
	 * @return
	 * @throws RevolutMoneyException
	 */
	@PUT
	@Path("/update/{userId}")
	public Response updateUser(@PathParam("userId") long userId, User user) throws RevolutMoneyException {
		log.info("Request Received to update User by Id " + userId);
		final int updateCount = daoFactory.getUserDAO().updateUser(userId, user);
		if (updateCount == 1) {
			return Response.ok("User updated successfully").build();
		} else {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}

	/**
	 * Delete by User Id
	 * 
	 * @param userId
	 * @return
	 * @throws RevolutMoneyException
	 */
	@DELETE
	@Path("/delete/{userId}")
	public Response deleteUser(@PathParam("userId") long userId) throws RevolutMoneyException {
		log.info("Request Received to delete User by Id " + userId);
		int deleteCount = daoFactory.getUserDAO().deleteUser(userId);
		if (deleteCount == 1) {
			return Response.ok("User deleted successfully").build();
		} else {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}

}
